<?php

/**
 * @file
 * Contains block_cache_metadata.module.
 */

use Drupal\block\Entity\Block;
use Drupal\block_cache_metadata\BlockCacheMetaDataHelper;
use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\Block\BlockPluginInterface;

/**
 * Implements hook_form_BASE_FORM_ID_alter().
 */
function block_cache_metadata_form_block_form_alter(&$form, FormStateInterface $form_state, $form_id) {
  /** @var \Drupal\block\Entity\Block $block */
  $block = $form_state->getFormObject()->getEntity();
  if ($block instanceof Block) {
    $module = 'block_cache_metadata';
    $cache_tags = NULL;
    $cache_contexts = NULL;
    $cache_max_age = NULL;
    if ($block_cache_metadata_settings = $block->getThirdPartySettings($module)) {
      if (isset($block_cache_metadata_settings['cache_tags'])) {
        $cache_tags = implode(',', $block_cache_metadata_settings['cache_tags']);
      }
      if (isset($block_cache_metadata_settings['cache_contexts'])) {
        $cache_contexts = implode(',', $block_cache_metadata_settings['cache_contexts']);
      }
      if (isset($block_cache_metadata_settings['cache_max_age'])) {
        $cache_max_age = $block_cache_metadata_settings['cache_max_age'];
      }
    }

    $form['settings']['block_cache_metadata_details'] = [
      '#type' => 'details',
      '#title' => t('Cache Metadata'),
      '#description' => t('Set cache metadata for this block. More on cache metadata you can find <a href="https://www.drupal.org/docs/8/api/cache-api">here</a>'),
    ];

    $form['settings']['block_cache_metadata_details']['cache_tags'] = [
      '#type' => 'textarea',
      '#title' => t('Cache tags'),
      '#description' => t('Add comma separated cache tags.'),
      '#default_value' => $cache_tags ? $cache_tags : '',
    ];

    $form['settings']['block_cache_metadata_details']['cache_max_age'] = [
      '#type' => 'number',
      '#title' => t('Cache max age'),
      '#description' => t('Set the cache max age for this block in seconds. -1 is no max age'),
      '#default_value' => $cache_max_age ? $cache_max_age : $block->getCacheMaxAge(),
    ];

    $form['settings']['block_cache_metadata_details']['cache_contexts'] = [
      '#type' => 'textarea',
      '#title' => t('Cache contexts'),
      '#description' => t('Add comma seperated cache contextss.'),
      '#default_value' => $cache_contexts ? $cache_contexts : '',
    ];

    BlockCacheMetaDataHelper::updateBlock($block, $form_state, $module);

  }
}

/**
 * Implements hook_block_build_alter().
 */
function block_cache_metadata_block_build_alter(array &$build, BlockPluginInterface $block) {
  $block_id = $build['#cache']['keys'][2];
  $block_entity = Drupal::entityTypeManager()->getStorage('block')->load($block_id);
  if ($block_entity instanceof Block) {
    $block_cache_metadata_settings = $block_entity->getThirdPartySettings('block_cache_metadata');
    if ($block_cache_metadata_settings) {
      if (isset($block_cache_metadata_settings['cache_contexts'])) {
        $build["#cache"]['contexts'] = array_merge($block->getCacheContexts(), $block_cache_metadata_settings['cache_contexts']);
      }
      if (isset($block_cache_metadata_settings['cache_tags'])) {
        $build["#cache"]['tags'] = array_merge($block->getCacheTags(), $block_cache_metadata_settings['cache_tags']);
      }
      if (isset($block_cache_metadata_settings['cache_max_age'])) {
        $build["#cache"]['max-age'] = $block_cache_metadata_settings['cache_max_age'];
      }
    }
  }
}
